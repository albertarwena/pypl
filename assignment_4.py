"""
Applied Plotting, Charting & Data Representation in Python
Assignmnet 4

Weather phenomena versur labor market statistics for the region of Ann Arbor, 
Michigan, United States.
This analysis is to investigate weather there is any signifficant relation between
weather phenomene and unemployment in Ann Arbor area. 
Yhe weather can impact labor makret, because of fluctuations in temporary employment in
agriculture and tourism. Althoug labour market fluctuates seasonally, it is probable, that
weather phenomena like hight percipitation of unusual temeperatures can have additional 
impact on the labour market.

Unemployment data for Ann Arbor downloaded from Michigan Department of Technology, 
Management and Budget.
http://milmi.org/datasearch

Weather data dor Ann Arbor region downloaded from National Oceanic and Atmospheric 
Administration website.
https://www.ncdc.noaa.gov/
Global Summary of the Month
Docummentation for the data set can be found:
https://www1.ncdc.noaa.gov/pub/data/cdo/documentation/gsom-gsoy_documentation.pdf
"""

import pandas as pd
from datetime import datetime

# Reading data as pandas data frames
labor_df = pd.read_excel('./data/ann_arbor_unemployment.xlsx', skiprows=1)
weather_df = pd.read_csv('./data/ann_arbor_monthly_weather.csv')

# Create valid time index for both data frames

# using datetime.strptime on integer representing date in YYYYMM format, 
# converting integer to string and parsing
labor_df['date_proper'] = labor_df['Period Value'].apply(lambda d: datetime.strptime(str(d), '%Y%m'))
weather_df['date_proper'] = weather_df['DATE'].apply(lambda d: datetime.strptime(d, '%Y-%m'))

# Calculate mean weather values from multiple weather stations, grouping variable set as
# data frame index, this is also a way of dealing with a missing data as they are not included
# in mean calculation
cols = ['PRCP', 'TAVG', 'TMAX', 'TMIN', 'date_proper']
weather_mean = weather_df[cols].groupby('date_proper').mean()

# create unemployment data frame and set date_proper as index
unemp_df = labor_df[['date_proper','Unemployment Rate']]
unemp_df = unemp_df.set_index('date_proper')
# combine datasets by date_proper time index


merged = pd.merge(weather_mean, unemp_df, right_index=True, left_index=True)
merged = merged[merged.index.year>2009]

import matplotlib.pyplot as plt
f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)

# Unemployment vs Percipitation
ax1.plot(merged['Unemployment Rate'], 'b')
ax1.plot(merged['PRCP'], 'orange')
ax1.set_title('Unemplotment vs Percipitation')
ax1.set_yscale('log')
ax1.set_ylabel(r'$log$ Percipitation')

# Unemplotment vs Average Temperature
ax2.plot(merged['Unemployment Rate'], 'b')
ax2.plot(merged['TAVG'], 'r')
ax2.set_title(r'Unemployment vs Mean Temperature')
ax2.set_ylabel(r'Temperature ($^\circ$C)')

# Remove unnesesary plot elements
ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)
ax1.tick_params(axis=u'both', which=u'both',length=0)


ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)
ax2.tick_params(axis=u'both', which=u'both',length=0)