# Tutorial on using SODA api

import requests
import pandas as pd

apiurl = 'https://data.cityofchicago.org/resource/f7f2-ggz5.json?'
filter = {'fuel_type_code' : 'LPG'}

# get data to data fra,e
r = requests.get(apiurl, filter)
data = pd.DataFrame(r.json())
data.head()

# filter points within box
filter2 = {'$where' : 'within_box(location, 41.885001, -87.645939, 41.867011, -87.618516)'}

r2 = requests.get(apiurl, filter2)
data2 = pd.DataFrame(r2.json())
data2.head()

# filter with multiple conditions
filter3 = {'$where' : 'within_box(location, 41.885001, -87.645939, 41.867011, -87.618516) OR fuel_type_code=\'LPG\''}

r3 = requests.get(apiurl, filter3)
data3 = pd.DataFrame(r3.json())
data3.head()